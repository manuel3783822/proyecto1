from django.apps import AppConfig


class HomepageConfig(AppConfig):
    name = 'app.homepage'
