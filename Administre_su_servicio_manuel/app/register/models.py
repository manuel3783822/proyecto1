from django.db import models
from django.contrib.auth.models import AbstractUser
from django.forms import model_to_dict

from crum import get_current_request

from app.models import BaseModel

class User(AbstractUser):
    phone = models.CharField(max_length=50, verbose_name='Telefono')
    password2 = models.CharField(max_length=50, verbose_name='Repetir contraseña')
    token = models.UUIDField(primary_key=False, editable=False, null=True, blank=True)

    def toJSON(self):
        item = model_to_dict(self)
        item['groups'] = [{'id': g.id, 'name': g.name} for g in self.groups.all()]
        return item

    def get_group_session(self):
        try:
            request = get_current_request()
            groups = self.groups.all()
            if groups.exists():
                if 'group' not in request.session:
                    request.session['group'] = groups[0]
        except:
            pass
