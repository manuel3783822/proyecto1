from django.contrib import admin
from django.urls import path

from app.register.views import UserAccoutActivation, UserActivated, UserChangeGroup, UserNotActived, UserRegister


#agrupar mi listas
app_name='register'

urlpatterns = [
    path('',UserRegister.as_view(),name='register'),
    path('activate/', UserAccoutActivation.as_view(), name='activate'),
    path('no_actived/', UserNotActived.as_view(), name='no_activated'),
    path('activated/<str:token>/', UserActivated.as_view(), name='user_activated'),
    path('change/group/<int:pk>/', UserChangeGroup.as_view(), name='user_change_group'),
]