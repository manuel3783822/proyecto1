
import uuid
from django.forms import ModelForm
from django import forms
from app.register.models import User
from django.contrib.auth.models import Group


class UserForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs['autofocus'] = True

    class Meta:
        model = User
        fields = '__all__'
        widgets = {
            'first_name': forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese sus nombres',
                }
            ),
            'last_name': forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese sus apellidos',
                }
            ),

            'username': forms.EmailInput(  # USERNAME ES EL CORREO
                attrs={
                    'placeholder': 'Ingrese su email',
                }
            ),
            'phone': forms.NumberInput(
                attrs={
                    'placeholder': 'Ingrese su numero',
                }
            ),
            'password': forms.PasswordInput(render_value=True,
                attrs={
                    'placeholder': 'Ingrese su password',
                }
            ),
            'password2': forms.PasswordInput(render_value=True,
                attrs={
                    'placeholder': 'Repetir contraseña',
                }
            ),
        }
        exclude = ['user_permissions', 'last_login', 'date_joined', 'is_superuser', 'is_active', 'is_staff']


    def clean_password2(self):
        password = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('password2')
        if password != password2:
            raise ValueError('Contraseñas no coinciden')
        return password2


    def save(self, commit=True):
        data = {}
        form = super()
        us = super().save(commit = False)
        us.set_password(self.cleaned_data['password'])
        try:
            if form.is_valid():
                pwd = self.cleaned_data['password']
                u = form.save(commit=False)
                if u.pk is None:
                    u.set_password(pwd)
                else:
                    user = User.objects.get(pk=u.pk)
                    if user.password != pwd:
                        u.set_password(pwd)
                u.is_active = 0
                u.token = uuid.uuid4()
                u.save()
                u.groups.clear()
                for g in self.cleaned_data['groups']:
                    u.groups.add(g)
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data


class ActivationAccountForm(forms.Form):
    username = forms.CharField(widget=forms.EmailInput(attrs={
        'placeholder': 'Digite email registrado',
        'class': 'form-control',
        'autocomplete': 'off'
    }))

    def clean(self):
        cleaned = super().clean()
        if not User.objects.filter(username=cleaned['username']).exists():
            # self._errors['error'] = self._errors.get('error', self.error_class())
            # self._errors['error'].append('El usuario no existe')
            raise forms.ValidationError('El usuario no existe o registro incorrectamente su email')
        return cleaned

    def get_user(self):
        username = self.cleaned_data.get('username')
        return User.objects.get(username=username)
