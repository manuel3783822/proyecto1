import uuid
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, TemplateView, FormView, View
from django.contrib.auth.models import Group
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from django.forms import model_to_dict
from app.register.forms import ActivationAccountForm, UserForm

# IMPORTACIONES PARA ENVIAR EMAIL
from portalservicios.wsgi import *
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from django.template.loader import render_to_string
from app.register.models import User

import portalservicios.settings as setting
from portalservicios import settings
# --------

class UserRegister(CreateView):
    model = User
    template_name = 'register.html'
    form_class = UserForm
    success_url = reverse_lazy('register:activate')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de un Usuario - PortalServicios'
        context['entity'] = 'Usuarios'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        return context


class UserAccoutActivation(FormView):
    template_name = 'activation_email.html'
    form_class = ActivationAccountForm
    success_url = reverse_lazy(setting.LOGIN_REDIRECT_URL)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    
    # EMAIL CONFIRMATION
    def send_email_confirmation_account(self, user):
        data = {}
        try:
            URL = settings.DOMAIN if not settings.DEBUG else self.request.META['HTTP_HOST']
            user.token = uuid.uuid4()
            user.save()

            mailServer = smtplib.SMTP(settings.EMAIL_HOST, settings.EMAIL_PORT)
            mailServer.starttls()
            mailServer.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)

            email_to = user.username # ============> NORMALMENTE ES EL CAMPO EMAIL QUIEN HACE ESTA PARTE, PERO EN ESTE CASO EL USERNAME ES EL EMAIL
            mensaje = MIMEMultipart()
            mensaje['From'] = settings.EMAIL_HOST_USER
            mensaje['To'] = email_to
            mensaje['Subject'] = 'Activacion de cuenta'

            content = render_to_string('send_email.html', {
                'user': user,
                'link_resetpwd': 'http://{}/register/activated/{}/'.format(URL, str(user.token)), # EN ESTE CASO SE LLAMA 'link_resetpwd', PORQUE ASI ESTA EN EL TEMPLATE PERO ESTE ACTIVA LA CUENTA EN EL BOTON
                'link_home': 'http://{}'.format(URL),
                'principal_message': 'Verificar cuenta',
                'second_message': 'Por favor da click en el botón de abajo para verificar su cuenta',
                'button_message': 'Verificar',
            })
            mensaje.attach(MIMEText(content, 'html'))

            mailServer.sendmail(settings.EMAIL_HOST_USER,
                                email_to,
                                mensaje.as_string())
        except Exception as e:
            data['error'] = str(e)
        return data

        
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            form = ActivationAccountForm(request.POST)  # self.get_form()
            if form.is_valid():
                user = form.get_user()
                data = self.send_email_confirmation_account(user)
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)


class UserActivated(TemplateView):
    template_name = 'success_activation.html'
    success_url = reverse_lazy(setting.LOGIN_REDIRECT_URL)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        token = self.kwargs['token']
        if User.objects.filter(token=token).exists():
            return super().get(request, *args, **kwargs)
        return HttpResponseRedirect('/')

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            user = User.objects.get(token=self.kwargs['token'])
            user.is_active = 1
            user.token = uuid.uuid4()
            user.save()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Activacion exitosa'
        context['login_url'] = settings.LOGIN_URL
        return context


class UserNotActived(TemplateView):
    template_name = 'activation_failed.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Cuenta sin activar'
        return context


class UserChangeGroup(LoginRequiredMixin, View):  
    def get(self, request, *args, **kwargs):
        try:
            request.session['group'] = Group.objects.get(pk=self.kwargs['pk'])
        except:
            pass
        return HttpResponseRedirect(reverse_lazy('portal:buyList'))