# Generated by Django 3.2.5 on 2022-03-17 21:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0008_user_is_email_verified'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='is_email_verified',
        ),
    ]
