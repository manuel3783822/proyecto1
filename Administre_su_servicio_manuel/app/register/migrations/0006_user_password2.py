# Generated by Django 3.2.5 on 2022-03-15 21:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0005_remove_user_contact_phone'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='password2',
            field=models.CharField(default=1, max_length=50, verbose_name='Repetir contraseña'),
            preserve_default=False,
        ),
    ]
