from datetime import datetime

from django import forms
from django.forms import ModelForm

from app.portal.models import Buy
from app.register.models import User
# from crum import get_current_user


class BuyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['plan'].widget.attrs['autofocus'] = True

    class Meta:
        model = Buy
        fields = '__all__'
        widgets = {
            # 'is_actived': forms.CheckboxInput(
            #     attrs={

            #         'class': 'select2',
            #     }
            # ),
            'user_creation': forms.HiddenInput(
                attrs={
                    'class': 'select2',
                }
            ),
            'unit': forms.Select(
                attrs={
                    'placeholder': 'Seleccione unidad',
                    'class': 'select2',
                }
            ),
            'no_unit': forms.NumberInput(
                attrs={
                    'placeholder': '#',
                }
            ),
            'plan': forms.Select(
                attrs={
                    'placeholder': 'Escoger plan',
                    'class': 'select2',
                }
            ),

            # ADJUNTAR ARCHIVOS
            'ad_cc': forms.FileInput(
                attrs={
                    'id': 'customFile'
                }
            ),
            'rec_pub': forms.FileInput(
                attrs={
                    'id': 'customFile1'
                }
            ),
            'signed_contract': forms.FileInput(
                attrs={
                    'placeholder': 'Ingrese su dirección',
                    'id': 'customFile2'
                }
            ),
            'observations': forms.Textarea(
                attrs={
                    'placeholder': 'Digite sus observaciones',
                    'rows': 3,
                    'cols': 3
                }
            )
        }


    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                instance = form.save()
                data = instance.toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data