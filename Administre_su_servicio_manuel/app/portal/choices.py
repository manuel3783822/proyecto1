terminal_type = (
    ('Administracion','Administracion'),
    ('Almacen','Almacen'),
    ('Apartamento','Apartamento'),
    ('Cabaña','Cabaña'),
    ('Casa','Casa'),
    ('Local','Local'),
    ('Pasaje','Pasaje'),
    ('Oficina','Oficina'),
    ('Porteria','Porteria'),
    ('Salon','Salon'),
)

plan_examples = (
    ('Doble','Doble'),
    ('Triple','Triple'),
)