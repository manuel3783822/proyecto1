from django.views.generic import TemplateView, ListView, CreateView, UpdateView, DeleteView
from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.http.response import JsonResponse
from django.urls import *
from app.portal.forms import BuyForm
from app.portal.mixins import ValidatePermissionRequiredMixin

from app.portal.models import Buy

# IMPORTACIONES PARA ENVIAR EMAIL
from portalservicios.wsgi import *
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from django.template.loader import render_to_string

import portalservicios.settings as setting
from portalservicios import settings
# --------


# CAMBIAR POR ListView
class BuyListView(LoginRequiredMixin, ValidatePermissionRequiredMixin, ListView):
    model = Buy
    template_name = 'buy/list.html'
    template_name1 = 'administre/homeA.html'
    permission_required = 'portal.view_buy'
    success_url = reverse_lazy('buyList')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        request.user.get_group_session()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
            data = {}
            try:
                action = request.POST['action']
                if action == 'searchdata':
                    data = []
                    group = self.request.user.groups.filter(name='Usuario').exists()
                    if group:
                        for i in Buy.objects.filter(user_creation=request.user):
                            data.append(i.toJSON())
                    else:
                        for i in Buy.objects.all():
                            data.append(i.toJSON())
                else:
                    data['error'] = 'Ha ocurrido un error'
            except Exception as e:
                data['error'] = str(e)
            return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Mis solicitudes'
        context['create_url'] = reverse_lazy('portal:buyCreate')
        return context



class BuyCreateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, CreateView):
    model = Buy
    form_class = BuyForm
    template_name = 'buy/create.html'
    template_name1 = 'administre/homeA.html'
    success_url = reverse_lazy('portal:buyList')
    permission_required = 'portal.add_buy'
    url_redirect = success_url

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Solicitar un servicio'
        context['entity'] = 'Comprar'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        return context


class BuyUpdateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, UpdateView):
    model = Buy
    form_class = BuyForm
    template_name = 'buy/create.html'
    template_name1 = 'administre/homeA.html'
    success_url = reverse_lazy('portal:buyList')
    permission_required = 'portal.change_buy'
    url_redirect = success_url

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)


   # EMAIL CONFIRMATION
    def send_email_state(self, u):
        data = {}
        try:
            URL = settings.DOMAIN if not settings.DEBUG else self.request.META['HTTP_HOST']
            mailServer = smtplib.SMTP(settings.EMAIL_HOST, settings.EMAIL_PORT)
            mailServer.starttls()
            mailServer.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)

            
            email_to = u.username # ============> NORMALMENTE ES EL CAMPO EMAIL QUIEN HACE ESTA PARTE, PERO EN ESTE CASO EL USERNAME ES EL EMAIL
            mensaje = MIMEMultipart()
            mensaje['From'] = settings.EMAIL_HOST_USER
            mensaje['To'] = email_to
            mensaje['Subject'] = 'Se ha actualizado tu solicitud'

            content = render_to_string('send_email.html', {
                'user': 'Hola ' + u.first_name + ' ' + u.last_name,
                'link_resetpwd': 'http://{}/portal/buy/list/'.format(URL),
                'principal_message': 'Se ha actualizado tu solicitud',
                'second_message': 'Tu informacion ha sido actualizada satisfactoriamente, verifica que tu estado de solicitud esta en "OK" y recuerda que si hiciste algun cambio despues de aceptada tu solicitud el estado se cambiara nuevamente a "POR ATENDER", gracias por elegirnos',
                'button_message': 'Ir al portal',
            })
            mensaje.attach(MIMEText(content, 'html'))

            mailServer.sendmail(settings.EMAIL_HOST_USER,
                                email_to,
                                mensaje.as_string())
        except Exception as e:
            data['error'] = str(e)
        return data


    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                user = self.get_object()
                u = user.user_creation
                data = self.send_email_state(u)
                data = form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    # def post(self, request, *args, **kwargs):
    #     data = {}
    #     try:
    #         action = request.POST['action']
    #         if action == 'edit':
    #             form = self.get_form()
    #             data = form.save()
    #         else:
    #             data['error'] = 'No ha ingresado a ninguna opción'
    #     except Exception as e:
    #         data['error'] = str(e)
    #     return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de una solicitud'
        context['entity'] = 'Solicitud'
        context['list_url'] = self.success_url
        context['action'] = 'edit'
        return context


class BuyDeleteView(LoginRequiredMixin, ValidatePermissionRequiredMixin, DeleteView):
    model = Buy
    template_name = 'buy/delete.html'
    success_url = reverse_lazy('portal:buyList')
    template_name1 = 'administre/homeA.html'
    permission_required = 'portal.delete_buy'
    url_redirect = success_url

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminar solicitud'
        context['entity'] = 'Venta'
        context['list_url'] = self.success_url
        return context