from django.contrib import admin
from django.urls import path
from app.portal.views.buy.views import BuyCreateView, BuyDeleteView, BuyListView, BuyUpdateView


#agrupar mi listas
app_name='portal'

urlpatterns = [
    path('buy/list/',BuyListView.as_view(),name='buyList'),
    path('buy/add/',BuyCreateView.as_view(),name='buyCreate'),
    path('buy/update/<int:pk>',BuyUpdateView.as_view(),name='buyUpdate'),
    path('buy/delete/<int:pk>',BuyDeleteView.as_view(),name='buyDelete'),
]