# Generated by Django 3.2.5 on 2022-03-24 16:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0006_alter_buy_user_created'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='buy',
            name='user_created',
        ),
    ]
