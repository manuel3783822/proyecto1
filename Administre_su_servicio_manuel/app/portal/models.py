
from django.db import models
from crum import get_current_user
from django.core.validators import RegexValidator
from django.forms import model_to_dict
from app.models import BaseModel
from app.portal.choices import terminal_type, plan_examples
from portalservicios.settings import AUTH_USER_MODEL

#EMAIL
# import smtplib
# from email.mime.multipart import MIMEMultipart
# from email.mime.text import MIMEText

# from django.template.loader import render_to_string
# from app.register.models import User

# from portalservicios import settings


class Buy(BaseModel):
    is_actived = models.BooleanField(default = False, null=False, blank=False, verbose_name='Estado')
    unit = models.CharField(max_length=50, null=False, blank=False, choices=terminal_type, verbose_name='Selecciona unidad')
    no_unit = models.CharField(max_length=50, verbose_name='Numero unidad')

    # PLAN
    plan = models.CharField(max_length=50, null=False, blank=False, choices=plan_examples, verbose_name='Plan') #SIGT SOAP
    
    # ADJUNTAR DOCUMENTOS
    ad_cc = models.CharField(max_length=500, null=True, blank=True, verbose_name='Adjuntar documento')
    rec_pub = models.CharField(max_length=500, null=True, blank=True, verbose_name='Recibos publico')
    signed_contract = models.CharField(max_length=500, null=True, blank=True, verbose_name='Contrato firmado') 
    
    observations = models.CharField(max_length=50, verbose_name='Observaciones')


    def __str__(self):
        return 'CLIENTE: ' + self.user_creation.username
        # return 'CLIENTE: ' + self.plan

    def save(self,force_insert=False,force_update=False,using=None,update_fields=None):
        user = get_current_user()
        if user is not None:
            if not self.pk:
                self.user_creation = user
            else:
                self.user_updated = user
        super(Buy,self).save()

    def activation_state(self):
        if self.is_actived is True:
            return 'OK'
        else:
            return 'POR ATENDER'

    def full_unit(self):
        return self.unit + ' #' + self.no_unit

    def toJSON(self):
        item = model_to_dict(self)
        item['full_unit'] = self.full_unit()
        item['state'] = self.activation_state()
        return item

    class Meta:
        verbose_name = 'Compra'
        verbose_name_plural = 'Compras'
