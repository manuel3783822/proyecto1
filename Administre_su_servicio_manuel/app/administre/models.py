from django.db import models

# Create your models here.


class DeviceFeatures(models.Model):
    def _modify_device(self, UPnP, password, **extra_fields):
        Administre = self.model(

            password = password,
            UPnP = UPnP,
            **extra_fields
        )
        Administre.set_puerto(UPnP)
        Administre.set_password(password)
        Administre.save(using=self.db)
        return Administre

    def _modify_device(self, UPnP, password=None, **extra_fields):
        return self._create_user(UPnP, password, False, False, **extra_fields)

class Device(models.Model):
    id = models.PositiveIntegerField(primary_key=True ,unique = True)
    ipAddress = models.GenericIPAddressField('Dirección ip',max_length = 15, unique = True,)
    serialNumber = models.IntegerField('Nuemro de serie', blank = True, null = True)
    modelName = models.CharField('Nombre del modelo', max_length = 30, blank = True, null = True)
    manufacturer = models.CharField('Provedor', max_length=30, null=True, blank = True)
    password = models.IntegerField('Contraseña')
    lastSessionTime = models.DateField(auto_now=False, auto_now_add=False)
    productClass = models.IntegerField(unique = True)
    UPnP = models.BooleanField()


    
  