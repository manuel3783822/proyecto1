#Primero se importa tanto la funcion path que es propidia de django.
#Llama tambien la función home de app.administre.views
from django.contrib import admin
from django.urls import path
from app.administre.api.api import Administre_API_View, Administre_update_API_View
from app.administre.api.views import DeviceList
from app.administre.api.views import home   
from app.portal.views.buy.views import BuyCreateView, BuyDeleteView, BuyListView, BuyUpdateView

#Luego se hace el path de la url para llamar a la vista.

urlpatterns = [
    
    # Pagina principal de los dispositivos
    
    path('apis/<int:pk>/',Administre_update_API_View, name = 'device_update-api'),
    path('apis/',Administre_API_View, name = 'device_api'),
    path('Device_List/',DeviceList.as_view(), name = 'device_unit'),
    
    # Pagina principal de la opcion administre
    path('home/',home, name = 'homeA'),

    # Pagina principal listas
    path('buy/list/',BuyListView.as_view(),name='buyList'),
    path('buy/add/',BuyCreateView.as_view(),name='buyCreate'),
    path('buy/update/<int:pk>',BuyUpdateView.as_view(),name='buyUpdate'),
    path('buy/delete/<int:pk>',BuyDeleteView.as_view(),name='buyDelete'),

]
