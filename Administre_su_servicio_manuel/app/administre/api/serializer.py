from rest_framework import serializers
from app.administre.models import Device


class DeviceSerializer(serializers.ModelSerializer): #Serializador basado en un modelo
    class Meta:
        model = Device 
        fields = '__all__'

# class DeviceSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Device
#         fields = ('id','ipAddress','serialNumber','modelName','manufacturer')