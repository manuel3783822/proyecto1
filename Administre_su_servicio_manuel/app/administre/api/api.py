from rest_framework import status
from urllib import request, response
from rest_framework.decorators import api_view
from rest_framework.response import Response
import requests
from rest_framework.views import APIView # ApiView es una clase que viene de rest framework 
from app.administre.api.serializer import DeviceSerializer  
from app.administre.models import Device
from django.shortcuts import render
from django.http import HttpResponse

# I have tried with three different certificates available. 


@api_view(['GET', 'POST'])
def Administre_API_View(request):

# Serializa el modelo de objetos a JSON
    if request.method == 'GET':
        dispositivo = Device.objects.all()
        device_serializer = DeviceSerializer(dispositivo,many = True)
        return Response(device_serializer.data,status = status.HTTP_200_OK)

# Deserializa el JSON a un modelo de objetos para comparar y mostrar
    elif request.method == 'POST':
        device_serializer =DeviceSerializer(data = request.data)
        
        
        if device_serializer.is_valid():
            device_serializer.save()
            return Response({'message: Dispositivo actualizado correctamente'},status = status.HTTP_201_CREATED)

        return Response(device_serializer.errors)

# Actualiza los datos y los muestra en la planilla
@api_view(['GET', 'PUT'])
def Administre_update_API_View(request, pk=None):


    if request.method == 'GET':
        dispositivo = Device.objects.filter(id = pk).first()
        device_serializer = DeviceSerializer(dispositivo)
        return Response(device_serializer.data, status = status.HTTP_200_OK)
    
    elif request.method == 'PUT':
        dispositivo = Device.objects.filter(id=pk).first()
        device_serializer = DeviceSerializer(dispositivo, data=request.data)
        if device_serializer.is_valid():
            device_serializer.save()
            return Response(device_serializer.data, status = status.HTTP_200_OK)
        return Response(device_serializer.errors, status = status.HTTP_400_BAD_REQUEST)


