from django.shortcuts import render
from app.portal.views.buy.views import *
from sre_parse import State
from rest_framework import generics
from app.administre.models import Device
from app.administre.api.serializer import DeviceSerializer
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView


# Aqui en la vista se usa la libreria render, que muestra información de los templates.

class DeviceList(generics.ListAPIView):
    Serializer_class = DeviceSerializer

    def get_queryset(self):
        return Device.objects.filter(State = True)


def home(request):
    
    return render(request,'administre/homeA.html')

from app.administre.api.serializer import DeviceSerializer

