from email.policy import default
from django.apps import AppConfig
from django.contrib import admin


class AdministreConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'administre'
    default = False
