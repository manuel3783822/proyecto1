"""portalservicios URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# santiago ramirez

from django import urls
from django.contrib import admin
from django.urls import path, include, re_path
from app.homepage.views import IndexView
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
   openapi.Info(
      title="UMP REST API",
      default_version=' 3.1.0',
      description="Test description",
      terms_of_service="https://emcali.avsystem.io:8087/doc/ump/v3",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=[permissions.AllowAny],
)


urlpatterns = [

    # FORMS_AND_HOMEPAGE
    path('admin/', admin.site.urls),
    path('', IndexView.as_view(), name='index'),
    path('login/',include('app.login.urls')),
    path('register/',include('app.register.urls')),
    path('portal/',include('app.portal.urls')),
    re_path(r'^doc/ump/v3(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    re_path(r'^doc/ump/v3/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    re_path(r'^doc/ump/v3/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
   

    #Aqui se crea un path que contiene 3 atributos
    #1. El campo que uno va recibir en la tabla de direcciones al que va acceder.
    #2. Luego accede a la funcion include que llama a todas las urls.
    #3. Un identificador para toda la la url.

    path('apis/',include('app.administre.urls')),

    path('administre/', include(('app.administre.urls'))),
    
    
]
